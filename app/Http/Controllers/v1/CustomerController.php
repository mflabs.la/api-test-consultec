<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Customer;
use App\Cities;
use App\Status;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Customer::with('city', 'status')->where('idstate', 1)->orderBy('id', 'DESC')->get();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function inactive(Request $request)
    {
        $data = Customer::find($request->idcustomer);
        if($data){
            $data->idstate = $request->status;
            if($data->save()){
                    return ['status' => true, 'data' => $data ];
                }else
                    return ['status' => false];
        }else
            return ['error-id' => false];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'idstate' => 'required',
            'idcity' => 'required'
        ]);
        if($validator->fails())
            return response($validator->errors(), 400);
    	$data = new Customer();
        $data->name = $request->name;
        $data->surname = $request->surname;
        $data->email = $request->email;
        $data->idstate = $request->idstate;
        $data->idcity = $request->idcity;
        if($data->save()){
            return ['status' => true, 'data' => $data ];	      
        }else
            return ['status' => false];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Customer::find($request->id);
        if($data){
            $data->name = $request->name;
            $data->surname = $request->surname;
            $data->email = $request->email;
            $data->idstate = $request->idstate;
            $data->idcity = $request->idcity;
            if($data->save()){
                    return ['status' => true, 'data' => $data ];
                }else
                    return ['status' => false];
        }else
            return ['error-id' => false];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
