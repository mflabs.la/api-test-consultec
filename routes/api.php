<?php

use Illuminate\Http\Request;

Route::group([ 'prefix' => 'v1'], function () {
	
	Route::resource('city', 'v1\UtilController');
	Route::resource('customer', 'v1\CustomerController');
	Route::put('update-customer', 'v1\CustomerController@update');
	Route::post('inactive', 'v1\CustomerController@inactive');
	Route::get('get-status', 'v1\UtilController@getStatus');


});